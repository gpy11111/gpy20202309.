public class BSTNode {
    int value;//存储的元素值
    BSTNode left;//左子树
    BSTNode right;//右子树
    private int height;//高度
    int leftsize;//左子树节点数

    public BSTNode(int value) {//构造方法
        this.value = value;
    }

        //添加节点（不考虑平衡）
        public void add11(BSTNode node){
        if (node==null){//待添加的节点为空，就直接return
            return;
        }
        //判断传入的节点的值，和当前的节点的值之间的关系
        if (node.value<this.value){//要插入节点的值<当前节点的值，说明该节点要插到当前节点的左子树

            if (this.left==null){//如果当前节点的左子节点为空，直接把插入的节点作为当前节点的左子树
                this.left=node;
            }else {//如果当前节点的左子节点不为空，递归向左子树添加
                this.left.add11(node);
            }
        }
        else if(node.value>this.value){//要插入节点的值>当前节点的值,，说明该节点要插到当前节点的右子树
            if (this.right==null){//如果当前节点的右子节点为空，直接把插入的节点作为当前节点的右子树
                this.right=node;
            }else {//如果当前节点的左子节点不为空，递归向右子树添加
                this.right.add11(node);
            }
        }
    }

        /**
     *插入新数据
     */
        public BSTNode insert(int x, BSTNode t) {
            if (t == null) {
                t= new BSTNode(x);
            }
            //比较插左边还是插右边
            else if (x < t.value) {//插到左子树上
                t.left = insert(x, t.left);
                //插入之后判断是否打破平衡
                //左子树打破平衡，用左子树的高减去右子树的高
                if (height(t.left) - height(t.right) ==2) {
                    //如果等于2，平衡被打破，需要进行调整。
                    if (x < t.left.value) {
                        //如果x小于t的左子树的值，x被插到t的左子树的左子树上，符合LL 用右旋转调整。
                        t = rightRotate(t);
                    } else {
                        //如果x大于t的左子树的值，x被插到t的左子树的右子树上，符合LR，用先左旋转后右旋转来矫正。
                        t = leftAndRightRotate(t);
                    }
                }
            } else if (x > t.value) {//插到右子树上，逻辑同上。
                t.right = insert(x, t.right);
                if (height(t.right) - height(t.left) ==2) {
                    if (x>t.right.value) {
                        t = leftRotate(t);
                    } else {
                        t = rightAndLeftRotate(t);
                    }
                }
            }
            t.height = Math.max(height(t.left), height(t.right)) + 1;
            return t;
        }

        /**
         *删除数据
         */
        public BSTNode remove(int x, BSTNode t) {
            if (t == null)
                return null;
            if (x<t.value) {
                t.left = remove(x, t.left);
                //删除之后验证该子树是否平衡
                if (t.right != null) {        //若右子树为空，则一定是平衡的，此时左子树相当对父节点深度最多为1, 所以只考虑右子树非空情况
                    if (t.left == null) {     //若左子树删除后为空，则需要判断右子树
                        if (height(t.right) - t.height >1) {
                            BSTNode k = t.right;
                            if (k.right != null) {        //右子树的右子树存在，按正常情况单旋转
                                t = leftRotate(t);
                            } else {                      //否则是右左情况，双旋转
                                t = rightAndLeftRotate(t);
                            }
                        }
                    }
                    if (t.left!=null){                  //否则判断左右子树的高度差
                        //左子树自身也可能不平衡，故先平衡左子树，再考虑整体
                        BSTNode k = t.left;
                        //右子树上最小节点补删除的节点
                        //k的左子树高度不低于k的右子树
                        if (k.right != null) {
                            if (height(k.left) - height(k.right) >1) {
                                BSTNode m = k.left;
                                if (m.left != null) {     //左子树的左子树存在，按正常情况单旋转
                                    rightRotate(k);
                                } else {                    //否则是左右情况，双旋转
                                    leftAndRightRotate(k);
                                }
                            }
                        } else {
                            if (height(k.left) - k.height >1) {
                                BSTNode m = k.left;
                                if (m.left != null) {     //左子树的左子树存在，按正常情况单旋转
                                    rightRotate(k);
                                } else {                      //否则是左右情况，双旋转
                                    leftAndRightRotate(k);
                                }
                            }
                        }
                        if (height(t.right) - height(t.left) >1) {
                            //右子树自身一定是平衡的，左右失衡的话单旋转可以解决问题
                            t = leftRotate(t);
                        }
                    }
                }
                //完了之后更新height值
                t.height = Math.max(height(t.left), height(t.right)) + 1;
            }
            else if (x>t.value) {
                t.right = remove(x, t.right);
                //下面验证子树是否平衡
                if (t.left != null) {  //若左子树为空，则一定是平衡的，此时右子树相当对父节点深度最多为1
                    t = balanceChild(t);
                }
                //更新height值
                t.height = Math.max(height(t.left), height(t.right)) + 1;
            }
            else if (t.left != null && t.right != null) {
                //用其右子树的最小数据代替该节点的数据并递归的删除那个节点
                BSTNode min = t.right;
                while (min.left != null) {
                    min = min.left;
                }
                // t.element = findMin(t.right).element;
                t.value = min.value;
                t.right = remove(t.value, t.right);
                t = balanceChild(t);
                //更新height值
                t.height = Math.max(height(t.left), height(t.right)) + 1;
            }
            else {
                t = (t.left != null) ? t.left : t.right;
            }
            return t;
        }

        /**
         * 平衡子树
        */
        private BSTNode balanceChild(BSTNode t) {
            if (t.right == null) {        //若右子树为空，则只需判断左子树与根的高度差
                if (height(t.left) - t.height >1) {
                    BSTNode k = t.left;
                    if (k.left != null) {
                        t = rightRotate(t);
                    } else {
                        t = leftAndRightRotate(t);
                    }
                }
            } else {              //若右子树非空，则判断左右子树的高度差
                //右子树可能不平衡，故先平衡右子树，再考虑整体
                BSTNode k = t.right;
                if (k.left != null) {
                    if (height(k.right) - height(k.left) >1) {
                        BSTNode m = k.right;
                        if (m.right != null) {        //右子树的右子树存在，按正常情况单旋转
                            leftRotate(k);
                        } else {                      //否则是右左情况，双旋转
                            rightAndLeftRotate(k);
                        }
                    }
                } else {
                    if (height(k.right) - k.height >1) {
                        BSTNode m = k.right;
                        if (m.right != null) {        //右子树的右子树存在，按正常情况单旋转
                            leftRotate(k);
                        } else {                      //否则是右左情况，双旋转
                            rightAndLeftRotate(k);
                        }
                    }
                }
                //左子树自身一定是平衡的，左右失衡的话单旋转可以解决问题
                if (height(t.left) - height(t.right) >1) {
                    t = rightRotate(t);
                }
            }
            return t;
        }

        /**
     *  查找
     */
        public boolean contains(int x, BSTNode tree) {
        if (tree == null) {
            return false;
        }
        if (x < tree.value) {
            return contains(x, tree.left);
        }
        else if (x > tree.value) {
            return contains(x, tree.right);
        }
        else {
            return true;
        }
    }

        /**
         * 右旋转
         */
        private BSTNode rightRotate(BSTNode t) {
            BSTNode newTree = t.left;
            t.left = newTree.right;
            newTree.right = t;
            t.height = Math.max(height(t.left), height(t.right)) + 1;
            newTree.height = Math.max(height(newTree.left), height(newTree.right)) + 1;
            System.out.print("-->");
            newTree.printTree(newTree);
            System.out.println(" ");

            return newTree;
        }
        /**
         * 左旋转
         */
        private BSTNode leftRotate(BSTNode t) {
            BSTNode newTree = t.right;
            t.right = newTree.left;
            newTree.left = t;
            t.height = Math.max(height(t.left), height(t.right)) + 1;
            newTree.height = Math.max(height(newTree.left), height(newTree.right)) + 1;
            System.out.print("-->");
            newTree.printTree(newTree);
            System.out.println(" ");
            return newTree;
        }
        /**
         * 先左旋后右旋
         */
        private BSTNode leftAndRightRotate(BSTNode t) {
            t.left = leftRotate(t.left);
            return rightRotate(t);
        }
        /**
         * 先右旋后左旋
         */
        private BSTNode rightAndLeftRotate(BSTNode t) {
            t.right = rightRotate(t.right);
            return leftRotate(t);
        }

        /**
         * 获取指定树的高度
         */
        private int height(BSTNode t) {
            return t == null ? -1 : t.height;
        }

        //先序遍历
        public void printTree(BSTNode tree) {
            if (tree == null) {
                return;
            }
            System.out.print(tree.value + " ");
            printTree(tree.left);
            printTree(tree.right);
        }

        //中序遍历
        public void printTree2(BSTNode tree){
            if(tree==null)
                return;
            printTree2(tree.left);
            System.out.println(tree.value +"------索引值："+tree.leftsize);
            printTree2(tree.right);
        }

        //节点个数
        private int getnum(BSTNode t){
            if(t!=null)//不为空树
                return getnum(t.left)+getnum(t.right)+1;
            else //为空树
                return 0;
        }

        //加上索引值
        public void leftsearch(BSTNode t){
            if(t!=null&&t.left!=null&&t.right!=null){
                t.leftsize=getnum(t.left);
                t.left.leftsearch(this);
                t.right.leftsearch(this);
            }
            else if(t!=null&&t.left==null&&t.right!=null){
                t.leftsize=getnum(t.left);
                t.right.leftsearch(this);
            }
            else if(t != null && t.left != null){
                t.leftsize=getnum(t.left);
                t.left.leftsearch(this);
            }
        }

    /**
     * 带索引的查找
     */
    public BSTNode index_contains(int x, BSTNode t){
        if(t!=null){
            //索引匹配
            if(x==t.leftsize)
                return t;
            //在左子树中查找
            else if(x<t.leftsize)
                return index_contains(x,t.left);
            //在右子树中查找
            else
                //在右子树里递归查找同时编号的值需要减去（leftsize+1）即左子树节点加上根节点
                return index_contains(x-t.leftsize-1,t.right);
        }
        else
            return t;
    }

    /*public BSTNode index_insert(int x,BSTNode t){

    }

    public BSTNode index_remove(int x,BSTNode t){

    }*/
}

