import java.util.*;
public class BST {

    private static BSTNode root;//树的根节点

    //添加节点
    public void add1(BSTNode node) {//不考虑平衡
        if (root == null) {
            root = node;//如果root为空，直接让root指向node
        } else {
            root.add11(node);
        }
    }

    // 前序遍历,"中左右"
    public void preOrder(BSTNode root) {
        /*
        if (root != null) {
            System.out.print(root.value + " ");
            preOrder(root.left);
            preOrder(root.right);
        }*/
        root.printTree(root);
    }

    //左子树高度
    public int getleftDepth(BSTNode root) {
        //如果根结点为空
        if (null == root) {
            return 0;
        } else {
            //递归调用
            //获取左孩子的深度
            int left_height = getleftDepth(root.left);
            return left_height + 1;// 只要左边孩子或者右边孩子不为空则深度+1
        }
    }
    //右子树高度
    public int getrightDepth(BSTNode root) {
        //如果根结点为空
        if (null == root) {
            return 0;
        } else {
            //递归调用
            //获取右孩子的深度
            int right_height = getrightDepth(root.right);
            return right_height + 1;// 只要左边孩子或者右边孩子不为空则深度+1
        }
    }

    //判断AVL
    public void judge_avl(BSTNode root) {
        int jud = Math.abs(getleftDepth(root) - getrightDepth(root));
        if (jud <= 1) {
            System.out.println("是AVL树");
        } else {
            System.out.println("不是AVL树");
        }
    }

    //插入
    public void insert(BST binarySortTree){
        Scanner scan= new Scanner(System.in);
        System.out.print("插入一个值： ");
        int n=scan.nextInt();//输入一个节点值
        root=root.insert(n, root);//进行插入
        System.out.print("即树为：");
        binarySortTree.preOrder(root);//先序遍历查看
    }

    //删除
    public void remove(BST binarySortTree) {
        Scanner scan= new Scanner(System.in);
        System.out.print("删除一个值： ");
        int n=scan.nextInt();//输入一个节点值
        root.remove(n, root);//进行删除
        System.out.print("即树为：");
        binarySortTree.preOrder(root);//先序遍历查看
    }

    //查找
    public void contains() {
        Scanner scan= new Scanner(System.in);
        System.out.print("查找一个值： ");
        int n=scan.nextInt();//输入一个节点值
        if(root.contains(n, root))
            System.out.println("找到");
        else
            System.out.println("未找到");
    }

    //索引输出(先序）
    public void index_printTree(BSTNode tree){
        if (tree != null) {
            System.out.println("节点值" + tree.value + "————索引值:" + tree.leftsize);
            index_printTree(tree.left);
            index_printTree(tree.right);
        }
    }

    //索引查找
    public void index_contains(){
        Scanner scan= new Scanner(System.in);
        System.out.print("输入中序遍历序号： ");
        int n=scan.nextInt();//输入序号
        root.leftsearch(root);//加上索引
        System.out.println("中序遍历：");
        root.printTree2(root);//中序遍历
        BSTNode bstNode = root.index_contains(n, root);//索引查找
        if(bstNode==null)
            System.out.println("未找到");
        else {
            System.out.print("找到：");
            System.out.println("节点是"+bstNode.value+"，索引值为"+bstNode.leftsize);
        }
    }

    //索引插入
    public void index_insert(BST binarySortTree){
        Scanner scan= new Scanner(System.in);
        System.out.print("插入一个值： ");
        int n=scan.nextInt();//输入一个节点值
        root=root.insert(n, root);//进行插入
        root.leftsearch(root);//加上索引
        binarySortTree.index_printTree(root);//索引输出
    }

    //索引删除
    public void index_remove(BST binarySortTree){
        Scanner scan= new Scanner(System.in);
        System.out.print("删除一个值： ");
        int n=scan.nextInt();//输入一个节点值
        root.remove(n, root);//进行删除
        root.leftsearch(root);//加上索引
        binarySortTree.index_printTree(root);//索引输出
    }


    public static void main(String[] args) {

        //构造一个二叉搜索树

        /*//键盘输入
        int[] arr = new int[100];
        Scanner k=new Scanner(System.in);
        int kk;
        System.out.println("输入树的节点值：");
        System.out.println("（输入-1结束）");
        for(int i=0;i<100;i++){
            kk=k.nextInt();
            if(kk!=-1){
                arr[i]=kk;
            }
            else
                break;
        }*/

        //预设数组
        int[] arr = {5,7,3,6,2};

        BST binarySortTree = new BST();
        for (int j : arr) {
            binarySortTree.add1(new BSTNode(j));
        }

        int select;
        do {
            //先序遍历一个二叉搜索树
            System.out.print("二叉树：");
            binarySortTree.preOrder(root);

            System.out.println(" ");
            System.out.println("1、判断是否是AVL树：");
            System.out.println("2、插入");
            System.out.println("3、删除");
            System.out.println("4、查找");
            System.out.print("输入要进行的操作的序号：");

            Scanner b = new Scanner(System.in);
            switch (b.nextInt()) {

                case 1:
                    System.out.println(" ");
                    System.out.println("AVL树的判断：");
                    binarySortTree.judge_avl(root);//判断AVL树
                    break;

                case 2:
                    System.out.println(" ");
                    System.out.println("AVL树的插入：");
                    System.out.println("选择：1、节点插入；2、索引插入");
                    System.out.print("请输入序号1或2：");
                    Scanner c = new Scanner(System.in);
                    int cc=c.nextInt();
                    if(cc==1){
                        System.out.println("AVL树节点插入：");
                        binarySortTree.insert(binarySortTree);//插入
                    }
                    else if(cc==2){
                        System.out.println("AVL树索引插入：");
                        binarySortTree.index_insert(binarySortTree);
                    }
                    else
                        System.out.println("输入错误");
                    break;

                case 3:
                    System.out.println(" ");
                    System.out.println("AVL树的删除：");
                    System.out.println("选择：1、节点删除；2、索引删除");
                    System.out.print("请输入序号1或2：");
                    Scanner e = new Scanner(System.in);
                    int ee=e.nextInt();
                    if(ee==1){
                        System.out.println("AVL树节点删除：");
                        binarySortTree.remove(binarySortTree);//删除
                    }
                    else if(ee==2){
                        System.out.println("AVL树索引删除：");
                        binarySortTree.index_remove(binarySortTree);
                    }
                    else
                        System.out.println("输入错误");
                    break;

                case 4:
                    System.out.println(" ");
                    System.out.println("AVL树的查找：");
                    System.out.println("选择：1、输入节点值查找；2、索引查找");
                    System.out.print("请输入序号1或2：");
                    Scanner d = new Scanner(System.in);
                    int dd=d.nextInt();
                    if(dd==1){
                        System.out.println("AVL树根据节点值查找：");
                        binarySortTree.contains();
                    }
                    else if(dd==2){
                        System.out.println("AVL树中序遍历序号查找：");
                        binarySortTree.index_contains();
                    }
                    else
                        System.out.println("输入错误");
                    break;

            }

            System.out.println(" ");
            System.out.println("输入1：继续");
            System.out.println("输入其他整数：结束");
            Scanner a = new Scanner(System.in);
            select = a.nextInt();
        }while (select==1);
    }
}
