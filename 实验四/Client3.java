import java.io.*;
import java.net.Socket;

public class Client3 {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("192.168.191.243",8800);
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        OutputStream outputStream = socket.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        String s= "( 1 + 2 i ) + ( 1 + -2 i )";
        int key=4;
        String es="";
        for(int i=0;i<s.length( );i++){
            char c=s.charAt(i);
            if(c>='a' && c<='z')
            {
                c+=key%26;
                if(c<'a') c+=26;
                if(c>'z') c-=26;
            }
            else if(c>='0' && c<='9')
            {
                c+=key%10;
                if(c<'0') c+=10;
                if(c>'9') c-=10;
            }
            else if(c>='*' && c<='/')
            {
                c+=key%6;
                if(c<'*') c+=6;
                if(c>'/') c-=6;
            }
            es+=c;
        }

        System.out.println(es);
        String info1 = es;
        outputStreamWriter.write(info1);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接受响应
        String reply = null;
        while (!((reply = bufferedReader.readLine())==null)){
            System.out.println(reply);
        }
        //关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        outputStream.close();
    }
}
