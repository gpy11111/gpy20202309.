import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server3 extends Wulishu{
    public static void main(String[] args) throws IOException {
        //建立服务器绑定窗口
        ServerSocket serverSocket = new ServerSocket(8800);
        //accept()方法处理连接请求，防止非法监听
        Socket socket = serverSocket.accept();
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        //输出流
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        //读取用户信息
        String info = null;
        System.out.println("服务器正在建立...");
        //反馈信息
        while (!((info=bufferedReader.readLine())==null)) {
            System.out.println("我是服务器Bei，接受的加密信息为：" + info);
            int key = -4;
            String es = "";
            for (int i = 0; i < info.length(); i++) {
                char c = info.charAt(i);
                if (c >= 'a' && c <= 'z')
                {
                    c += key % 26;
                    if (c < 'a') c += 26;  //向左超界
                    if (c > 'z') c -= 26;  //向右超界
                } else if (c >= '0' && c <= '9') // 是数字
                {
                    c += key % 10;
                    if (c < '0') c += 10;
                    if (c > '9') c -= 10;
                } else if (c >= '*' && c <= '/') {
                    c += key % 6;
                    if (c < '*') c += 6;
                    if (c > '/') c -= 6;
                }
                es += c;
            }
            System.out.println("解密后的信息为：" + es);

            Wulishu ff;
            ff = new Server3();
            ff.Faction(es);

            Wulishu count;
            count = new Server3();

            switch (ch){
                case '+':
                {
                    count.Add();
                    break;
                }
                case '-':
                {
                    count.Sub();
                    break;
                }
                case '*':
                {
                    count.Mul();
                    break;
                }
                case '/':
                {
                    count.Div();
                    break;
                }
            }

            String reply = count.toString(es);

            //传递信息
            printWriter.write(reply);
            printWriter.flush();
            //关闭资源
            inputStream.close();
            outputStream.close();
            bufferedReader.close();
            printWriter.close();
            serverSocket.close();
            socket.close();
        }
    }
}

