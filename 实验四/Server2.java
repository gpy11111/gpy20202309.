import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;
public class Server2 {
    private static fenshu frac2;
    private static fenshu frac1;
    private static String a,b;
    private static char ch;
    private static fenshu result = null;

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket=new ServerSocket(8809);
        Socket socket=serverSocket.accept();
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        String info=null;
        System.out.println("服务器已经建立......");
        while(!((info = bufferedReader.readLine()) ==null)){
            System.out.println("我是服务器，用户的加密信息为：" + info);
            String s= info;
            String es="";
            int key = -4;
            for(int i=0;i<s.length( );i++)
            {  char c=s.charAt(i);
                if(c>=0 && c<=127)
                { c+=key%26;
                    if(c<0) c+=127;
                    if(c>127) c-=127;
                }

                es+=c;
            }

            System.out.println("我是服务器，用户的解密后的信息为：" + es);
            StringTokenizer st = new StringTokenizer(es, " ", false);
            a=st.nextToken();
            ch=st.nextToken().charAt(0);
            b=st.nextToken();
            frac1=new fenshu(a);
            frac2=new fenshu(b);

            switch (ch)
            {
                case '+':
                    result=frac1.getJia(frac2);

                    break;
                case '-':
                    result=frac1.getJian(frac2);

                    break;
                case '*':
                    result=frac1.getCheng(frac2);

                    break;
                case '/':
                    result=frac1.getChu(frac2);

                    break;
                default:

                    break;
            }
        }
        //给客户一个响应
        String reply=frac1+String.valueOf(ch)+frac2+"="+result;
        String z= reply;
        String tes="";
        int key = 4;
        for(int j=0;j<z.length( );j++)
        {  char d=z.charAt(j);
            if(d>=0 && d<=127)
            { d+=key%26;
                if(d<0) d+=127;
                if(d>127) d-=127;
            }

            tes+=d;
        }
        printWriter.write(tes);
        printWriter.flush();
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}
