public class KaisaMima {
    public static void main(String[] args) throws Exception{
        String s="Hello World";
        int key=4;
        char[] chars=s.toCharArray();
        StringBuilder newchars=new StringBuilder();
        for(int i=0;i<s.length();i++) {
            int c=chars[i];
            if(c>='a' && c<='z') //小写
            { c+=key%26;         //移动位数
                if(c<'a') c+=26;
                if(c>'z') c-=26;
            }
            else if(c>='A' && c<='Z') //大写
            {  c+=key%26;
                if(c<'A') c+=26;
                if(c>'Z') c-=26;
            }
            char cc=(char)c;
            newchars.append(cc);
        }
        System.out.println(newchars);
    }
}
