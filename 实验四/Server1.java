import java.net.*;
import java.io.*;
public class Server1 {
    public static void main(String[] args) throws IOException {
        //建立服务器绑定窗口
        ServerSocket serverSocket = new ServerSocket(8800);
        //accept()方法处理连接请求，防止非法监听
        Socket socket = serverSocket.accept();
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        //输出流
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        //读取用户信息
        String message = null;
        System.out.println("服务器建立中");
        //反馈信息
        while (!((message=bufferedReader.readLine())==null)){
            System.out.println("这里是服务器，接受的信息为："+message);
        }

        String reply = "Hello!";
        //传递信息
        printWriter.write(reply);
        printWriter.flush();
        //关闭资源
        inputStream.close();
        outputStream.close();
        bufferedReader.close();
        printWriter.close();
        serverSocket.close();
        socket.close();
    }
}


