import java.net.*;
import java.io.*;

public class Client1 {
    public static void main(String[] args) throws IOException {
        //建立客户端,host为伙伴的IP地址
        Socket socket = new Socket("172.20.10.13",8800);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //输出流，获取socket读写流
        OutputStream outputStream = socket.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //利用流按照一定的操作，传递信息，对socket进行读写操作
        String message1 = "用户名：Username 密码：2023";
        outputStreamWriter.write(message1);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接受响应
        String reply = null;
        while (!((reply = bufferedReader.readLine())==null)){
            System.out.println(reply);
        }
        //关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        outputStream.close();
    }
}

