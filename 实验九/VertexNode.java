/*
 用于存储顶点对象的链表
 */
public class VertexNode {
    Vertex vertexData;
    VertexNode next;
    public VertexNode(Vertex ver){
        vertexData=ver;
    }
}