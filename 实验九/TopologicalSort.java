import java.util.ArrayList;
import java.util.Scanner;

public class TopologicalSort {
    int time;
    //  下面链表用于记录拓扑序列
    LinkList linkList=new LinkList();

    /*
     根据用户输入的string类型的顶点返回该顶点
     */
    public Vertex getVertex(Graph graph,String str){
        for(int i=0;i<graph.verNum;i++){
            if(graph.vertexArray[i].verName.equals(str)){
                return graph.vertexArray[i];
            }
        }
        return null;
    }

    /**
     * 根据用户输入的数据初始化一个图，以邻接表的形式构建!
     * @param graph 生成的图
     */
    public void initialGraph(Graph graph){
        @SuppressWarnings("resource")
        Scanner scan=new Scanner(System.in);
        System.out.println("请输入顶点数和边数：");
        graph.verNum=scan.nextInt();
        graph.edgeNum=scan.nextInt();

        System.out.println("请依次输入顶点名称：");
        for(int i=0;i<graph.verNum;i++){
            Vertex vertex=new Vertex();
            String name=scan.next();
            vertex.verName=name;
            vertex.color="white";
            vertex.discoverTime=0;
            vertex.finishTime=0;
            vertex.parent=null;
            vertex.nextNode=null;
            graph.vertexArray[i]=vertex;
        }

        System.out.println("请依次输入图的边(头节点 回车 尾节点)：");
        for(int i=0;i<graph.edgeNum;i++){
            String preV=scan.next();
            String folV=scan.next();
            System.out.println("---------");

            Vertex v1=getVertex(graph,preV);
            if(v1==null)
                System.out.println("输入边存在图中没有的顶点！");
            Vertex v2=new Vertex();
            v2.verName=folV;
            v2.nextNode=v1.nextNode;
            v1.nextNode=v2;
        }
    }

    /*
     输出图的邻接表
     */
    public void outputGraph(Graph graph){
        for(int i=0;i<graph.verNum;i++){
            Vertex vertex=graph.vertexArray[i];
            System.out.print(vertex.verName);

            Vertex current=vertex.nextNode;
            while(current!=null){
                System.out.print("-->"+current.verName);
                current=current.nextNode;
            }
            System.out.println();
        }
    }

    /*
     利用图的DFS遍历完成拓扑排序主函数
     */
    public void dfsTopoligical(Graph graph){
        for(int i=0;i<graph.verNum;i++){
            if(graph.vertexArray[i].color.equals("white")){
                topological(graph.vertexArray[i],graph);
                System.out.println();
            }
        }
    }

    /*
     利用DFS完成图的拓扑排序辅助函数
     */
    public void topological(Vertex vertex,Graph graph){
        vertex.color="gray";
        time=time+1;
        vertex.discoverTime=time;
        System.out.print(vertex.verName+"-->");

        Vertex current=vertex.nextNode;
        while(current!=null){
            Vertex currentNow=getVertex(graph, current.verName);
            if(currentNow.color.equals("white"))
                topological(currentNow,graph);
            current=current.nextNode;
        }
        vertex.color="black";
        time=time+1;
        vertex.finishTime=time;

        linkList.addFirstNode(vertex);
    }

    public static void main(String[] args) {
        TopologicalSort topologicalSort=new TopologicalSort();
        Graph graph=new Graph();
        topologicalSort.initialGraph(graph);
        System.out.println("输出图的邻接链表表示为：");
        topologicalSort.outputGraph(graph);

        System.out.println("\n输出图的DFS遍历结果为：");
        topologicalSort.dfsTopoligical(graph);

        System.out.println("\n图的拓扑排序结果为：");
        VertexNode current=topologicalSort.linkList.first;
        while(current!=null){
            System.out.print(current.vertexData.verName+"-->");
            current=current.next;
        }
        System.out.println("null");
    }
}