import java.util.*;

/*图的BFS遍历和DFS遍历:*/
public class CreateGraph1 {
    int time=0;
    Stack<Vertex> stackVertex=new Stack<Vertex>();

    public static void main(String[] args) {
        Graph graph=new Graph();
        CreateGraph1 createGraph=new CreateGraph1();
        createGraph.initialGraph(graph);
        createGraph.outputGraph(graph);
        System.out.println("通过DFS搜索路径(递归实现)输入：1");
        System.out.println("通过DFS搜索路径(栈实现)输入：2");
        System.out.println("通过BFS搜索路径输入：3");
        Scanner sc=new Scanner(System.in);
        int x=sc.nextInt();
        if(x==1){
          System.out.println("DFS搜索路径为(递归实现)：");
          createGraph.DFS(graph);
        }
        if(x==2){
          System.out.println("DFS搜索路径为(栈实现):");
          createGraph.stackMain(graph);
        }
        if(x==3){
          System.out.println("BFS搜索路径为：");
          createGraph.BFS(graph);}
    }

    /*
     根据用户输入的string类型的顶点返回该顶点
     */
    public Vertex getVertex(Graph graph,String str){
        for(int i=0;i<graph.verNum;i++){
            if(graph.vertexArray[i].verName.equals(str)){
                return graph.vertexArray[i];
            }
        }
        return null;
    }

    /*
     根据用户输入的数据初始化一个图，以邻接表的形式构建!
     */
    public void initialGraph(Graph graph){
        @SuppressWarnings("resource")
        Scanner scan=new Scanner(System.in);
        System.out.println("请输入顶点数和边数：");
        graph.verNum=scan.nextInt();
        graph.edgeNum=scan.nextInt();
        System.out.println("请输入无向图（1）或有向图（0）：");
        int choose=scan.nextInt();
        System.out.println("请依次输入顶点名称：");
        for(int i=0;i<graph.verNum;i++){
            Vertex vertex=new Vertex();
            String name=scan.next();
            vertex.verName=name;
            vertex.color="white";
            vertex.discoverTime=0;
            vertex.finishTime=0;
            vertex.nextNode=null;
            graph.vertexArray[i]=vertex;
        }

        System.out.println("请依次输入图的边(头节点 回车 尾节点)：");
        for(int i=0;i<graph.edgeNum;i++){
            String preV=scan.next();
            String folV=scan.next();
            System.out.println("---------");
            Vertex v1=getVertex(graph,preV);
            if(v1==null)
                System.out.println("输入边存在图中没有的顶点！");
//链表操作
            Vertex v2=new Vertex();
            v2.verName=folV;
            v2.nextNode=v1.nextNode;
            v1.nextNode=v2;
//下面的代码是构建无向图的
            if(choose==1){
                Vertex reV2=getVertex(graph,folV);
                if(reV2==null)
                    System.out.println("输入边存在图中没有的顶点！");
                Vertex reV1=new Vertex();
                reV1.verName=preV;
                reV1.nextNode=reV2.nextNode;
                reV2.nextNode=reV1;
            }
        }
    }

    /*
     输出图的邻接表
     */
    public void outputGraph(Graph graph){
        System.out.println("输出图的邻接链表为：");
        for(int i=0;i<graph.verNum;i++){
            Vertex vertex=graph.vertexArray[i];
            System.out.print(vertex.verName);

            Vertex current=vertex.nextNode;
            while(current!=null){
                System.out.print("-->"+current.verName);
                current=current.nextNode;
            }
            System.out.println();
        }
    }

    /*
     DFS遍历辅助函数，标记颜色是辅助，即根据顶点返回其下标
     */
    public int index(Vertex vertex,Graph graph){
        for(int i=0;i<graph.verNum;i++){
            if(vertex.verName.equals(graph.vertexArray[i].verName))
                return i;
        }
        return -1;
    }

    /*
     DFS深度优先遍历初始化
     */
    public void DFS(Graph graph){
        for(int i=0;i<graph.verNum;i++){
            if(graph.vertexArray[i].color.equals("white")){
                DfsVisit(graph.vertexArray[i],graph);
                System.out.println();
            }
        }
    }

    /*
     DFS递归函数
     */
    public void DfsVisit(Vertex vertex,Graph graph){
        vertex.color="gray";
        time=time+1;
        vertex.discoverTime=time;
        System.out.print(vertex.verName+"-->");

        Vertex current=vertex.nextNode;
        while(current!=null){
            Vertex currentNow=getVertex(graph, current.verName);
            if(currentNow.color.equals("white"))
                DfsVisit(currentNow,graph);
            current=current.nextNode;
        }
        vertex.color="black";
        time=time+1;
        vertex.finishTime=time;
    }

    /*
     寻找一个节点的邻接点中是否还有白色节点
     返回白色节点或是null
     */
    public Vertex getAdj(Graph graph,Vertex vertex){
        Vertex ver=getVertex(graph, vertex.verName);
        Vertex current=ver.nextNode;
        if(current==null)
            return null;
        else{
            Vertex cur=getVertex(graph, current.verName);
            while(current!=null && cur.color.equals("gray")){
                current=current.nextNode;
            }
            if(cur.color.equals("white")){
                Vertex currentNow=getVertex(graph, current.verName);
                return currentNow;
            }else{
                return null;
            }
        }

    }

    /*
     通过栈实现dfs遍历
     */
    public void stackOperator(Graph graph,Vertex vertex){
        vertex.color="gray";
        stackVertex.push(vertex);
        System.out.print(vertex.verName+"-->");

        while(!stackVertex.isEmpty()){
            Vertex ver=stackVertex.peek();
            Vertex current=getAdj(graph,ver);
            if(current!=null){
                stackVertex.push(current);
                current.color="gray";
                System.out.print(current.verName+"-->");
            }else{
                stackVertex.pop();
            }
        }
    }

    /*
     DFS遍历主函数
     */
    public void stackMain(Graph graph){
        for(int i=0;i<graph.verNum;i++){
            if(graph.vertexArray[i].color.equals("white")){
                stackOperator(graph,graph.vertexArray[i]);
                System.out.println();
            }
        }
    }

    /*
     BFS广度优先搜索实现
     */
    public void BFS(Graph graph){
        Vertex current=graph.vertexArray[0];
        current.color="gray";
        time=time+1;
        current.discoverTime=time;

        Queue<Vertex> queue=new LinkedList<Vertex>();
        queue.offer(current);
        while(queue.peek()!=null){
            Vertex ver=queue.poll();
            time=time+1;
            ver.finishTime=time;
            System.out.print(ver.verName+"-->");

            Vertex cur=ver.nextNode;
            while(cur!=null){
                Vertex curNow=getVertex(graph, cur.verName);
                if(curNow.color.equals("white")){
                    curNow.color="gray";
                    time=time+1;
                    curNow.discoverTime=time;
                    queue.offer(curNow);
                }
                cur=cur.nextNode;
            }
        }
        System.out.println("null");
    }
}
