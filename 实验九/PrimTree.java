import java.util.Scanner;

public class PrimTree {
    int currentSize=0;
    int maxSize=0;
    Vertex[] minHeap=new Vertex[20];

    public Vertex getVertex(Graph graph, String str) {
        for (int i = 0; i < graph.verNum; i++) {
            if (graph.vertexArray[i].verName.equals(str)) {
                return graph.vertexArray[i];
            }
        }
        return null;
    }

    /*
     根据用户输入的数据初始化一个图，以邻接表的形式构建!
     */
    public void initialGraph(Graph graph) {
        @SuppressWarnings("resource")
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入顶点数和边数：");
        graph.verNum = scan.nextInt();
        graph.edgeNum = scan.nextInt();

        System.out.println("请依次输入顶点名称：");
        for (int i = 0; i < graph.verNum; i++) {
            Vertex vertex = new Vertex();
            String name = scan.next();
            vertex.verName = name;
            vertex.color = "white";
            vertex.discoverTime = 0;
            vertex.finishTime = 0;
            vertex.parent = null;
            vertex.nextNode = null;
            graph.vertexArray[i] = vertex;
        }

        System.out.println("请依次输入图的边(头节点 尾节点 权值)：");
        for (int i = 0; i < graph.edgeNum; i++) {
            String preV = scan.next();
            String folV = scan.next();
            int weight=scan.nextInt();
            System.out.println("---------");
            Vertex v1=getVertex(graph,preV);
            if(v1==null)
                System.out.println("输入边存在图中没有的顶点！");
            Vertex v2=new Vertex();
            v2.verName=folV;
            v2.weight=weight;
            v2.nextNode=v1.nextNode;
            v1.nextNode=v2;

            Vertex reV2=getVertex(graph,folV);
            if(reV2==null)
                System.out.println("输入边存在图中没有的顶点！");
            Vertex reV1=new Vertex();
            reV1.verName=preV;
            reV1.weight=weight;
            reV1.nextNode=reV2.nextNode;
            reV2.nextNode=reV1;
        }
    }
    public void outputGraph(Graph graph){
        System.out.println("输出加权图的邻接链表:");
        for(int i=0;i<graph.verNum;i++){
            Vertex vertex=graph.vertexArray[i];
            System.out.print(vertex.verName);

            Vertex current=vertex.nextNode;
            while(current!=null){
                System.out.print("-->"+current.verName);
                current=current.nextNode;
            }
            System.out.println();
        }
    }
    /*
     通过weight构建以EdgeNode为节点的最小堆
     */
    public void createMinHeap(Vertex[] verArray){
        currentSize=verArray.length;
        maxSize=minHeap.length;
        if(currentSize>=maxSize){
            maxSize*=2;
            minHeap=new Vertex[maxSize];
        }
        for(int i=0;i<currentSize;i++)
            minHeap[i+1]=verArray[i];

        double y;
        int c;
        for(int i=currentSize/2;i>=1;i--){
            Vertex ver=minHeap[i];
            y=ver.key;
            c=2*i;
            while(c<=currentSize){
                if(c<currentSize && minHeap[c].key>minHeap[c+1].key)
                    c++;
                if(minHeap[c].key>=y)
                    break;
                minHeap[c/2]=minHeap[c];
                c=c*2;
            }
            minHeap[c/2]=ver;
        }
    }

    public Vertex deleteMinHeap(){
        if(currentSize<1)
            System.out.println("堆已经为空！无法执行删除");
        Vertex ver=minHeap[1];
        minHeap[1]=minHeap[currentSize];
        currentSize-=1;

        int c=2,j=1;
        Vertex ver1=minHeap[currentSize+1];
        while(c<=currentSize){
            if(c<currentSize && minHeap[c].key>minHeap[c+1].key)
                c++;
            if(ver1.key<=minHeap[c].key)
                break;
            minHeap[j]=minHeap[c];
            j=c;
            c=c*2;
        }
        minHeap[j]=ver1;
        return ver;
    }

    /*
     返回minHeap中的顶点对象
     */
    public Vertex getHeapVertex(String name){
        for(int i=1;i<=currentSize;i++){
            if(minHeap[i].verName.equals(name))
                return minHeap[i];
        }
        return null;
    }

    /*
      MST的Prim算法具体实现函数
     */
    public void primSpanningTree(Graph graph){
        System.out.println("请输入根节点：");
        @SuppressWarnings("resource")
        Scanner scan=new Scanner(System.in);
        String root=scan.next();
        Vertex verRoot=getVertex(graph,root);
        verRoot.key=0;

        Vertex[] verArray=new Vertex[graph.verNum];
        for(int i=0;i<graph.verNum;i++){
            verArray[i]=graph.vertexArray[i];
        }
        createMinHeap(verArray);

        System.out.println("利用prim算法依次加入到MST中的顶点顺序为:");
        while(currentSize>=1){

            Vertex[] vArray=new Vertex[currentSize];
            for(int i=0;i<currentSize;i++){
                vArray[i]=minHeap[i+1];
            }

            createMinHeap(vArray);
            Vertex u=deleteMinHeap();
            System.out.println(">."+u.verName);
            Vertex current=u.nextNode;

            while(current!=null){
                Vertex currentNow=getHeapVertex(current.verName);
                if(currentNow!=null && current.weight<currentNow.key){
                    currentNow.parent=u;
                    currentNow.key=current.weight;
                }
                current=current.nextNode;
            }
        }
    }

    public static void main(String[] args) {
        Graph graph=new Graph();
        PrimTree create=new PrimTree();
        create.initialGraph(graph);
        create.outputGraph(graph);
        PrimTree prim=new PrimTree();
        prim.primSpanningTree(graph);
    }
}