import java.util.Scanner;
public class CreateGraph {
    public Vertex getVertex(Graph graph, String str){
        for(int i=0;i<graph.verNum;i++){
            if(graph.vertexArray[i].verName.equals(str)){
                return graph.vertexArray[i];
            }
        }
        return null;
    }
    /*
     根据用户输入的数据初始化一个图，以邻接表的形式构建!
     */
    public void initialGraph(Graph graph){
        @SuppressWarnings("resource")
        Scanner scan=new Scanner(System.in);
        System.out.println("请输入顶点数和边数：");
        graph.verNum=scan.nextInt();
        graph.edgeNum=scan.nextInt();
        System.out.println("请输入无向图（1）或有向图（0）：");
        int choose=scan.nextInt();
        System.out.println("请依次输入顶点名称：");
        for(int i=0;i<graph.verNum;i++){
            Vertex vertex=new Vertex();
            String name=scan.next();
            vertex.verName=name;
            vertex.nextNode=null;
            graph.vertexArray[i]=vertex;
        }

        System.out.println("请依次输入图的边(头节点 回车 尾节点)：");
        for(int i=0;i<graph.edgeNum;i++){
            String preV=scan.next();
            String folV=scan.next();
            System.out.println("----------");
            Vertex v1=getVertex(graph,preV);
            if(v1==null)
                System.out.println("输入边存在图中没有的顶点！");
//链表操作
            Vertex v2=new Vertex();
            v2.verName=folV;
            v2.nextNode=v1.nextNode;
            v1.nextNode=v2;
//下面的代码是构建无向图的
          if(choose==1){
            Vertex reV2=getVertex(graph,folV);
            if(reV2==null)
              System.out.println("输入边存在图中没有的顶点！");
            Vertex reV1=new Vertex();
            reV1.verName=preV;
            reV1.nextNode=reV2.nextNode;
            reV2.nextNode=reV1;
          }
        }
    }

    public void outputGraph(Graph graph){
        System.out.println("输出图的邻接链表为：");
        for(int i=0;i<graph.verNum;i++){
            Vertex vertex=graph.vertexArray[i];
            System.out.print(vertex.verName);

            Vertex current=vertex.nextNode;
            while(current!=null){
                System.out.print("-->"+current.verName);
                current=current.nextNode;
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Graph graph=new Graph();
        CreateGraph createGraph=new CreateGraph();
        createGraph.initialGraph(graph);
        createGraph.outputGraph(graph);
    }
}