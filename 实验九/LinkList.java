public class LinkList {
    VertexNode first;

    /*
     构造函数，用于初始化头节点
     */
    public LinkList(){
        this.first=null;
    }

    /*
     向空链表中插入头结点
     */
    public void addFirstNode(Vertex vertex){
        VertexNode vertexNode=new VertexNode(vertex);
        vertexNode.next=first;
        first=vertexNode;
    }

    /*
     从链表尾部插入节点
     */
    public void addTailNode(Vertex vertex){
        VertexNode vertexNode=new VertexNode(vertex);
        VertexNode current=first;
        if(current==null){
            vertexNode.next=current;
            first=vertexNode;
        }else{
            VertexNode present=current;
            while(current!=null){
                present=current;
                current=current.next;
            }
            present.next=vertexNode;
        }
    }
}