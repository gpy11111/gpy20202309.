/*
 图的顶点类
 */
public class Vertex {
    String verName;
    Vertex nextNode;
    String color;
    int discoverTime;
    int finishTime;
    Vertex parent;
    int weight;
    double key;
}