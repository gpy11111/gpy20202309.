import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
   StringBuffer a=new StringBuffer("StringBuffer");
    StringBuffer b=new StringBuffer("StringBufferStringBuffer");
    StringBuffer c=new StringBuffer("StringBufferStringBufferStringBuffer");
    @Test
    public void testcharAt() throws Exception{
        assertEquals('S',a.charAt(0));
        assertEquals('g',a.charAt(5));
        assertEquals('r',a.charAt(11));
    }
    @Test
    public void testcapacity() throws Exception{
        assertEquals(20,a.capacity());
        assertEquals(40,b.capacity());
        assertEquals(52,c.capacity());
    }
    @Test
    public void testlength() throws Exception{
        assertEquals(12,a.length());
        assertEquals(24,b.length());
        assertEquals(36,c.length());
    }

}