abstract class Data {
    abstract public void DisplayValue();
}
class Byte extends Data {
    byte value;
    Byte() {
        value=20202309%6;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Short extends Data {
    short value;
    Short() {
        value=20202309%6;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Boolean extends Data {
    boolean value;
    Boolean() {
        value=20202309%6==9;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Long extends Data {
    long value;
    Long() {
        value=20202309;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Float extends Data {
    float value;
    Float() {
        value=20202309%6;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Double extends Data {
    double value;
    Double() {
        value=20202309%6;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}

class ByteFactory extends Factory {
    public Data CreateDataObject(){
        return new Byte();
    }
}
class ShortFactory extends Factory {
    public Data CreateDataObject(){
        return new Short();
    }
}
class BoolenFactory extends Factory {
    public Data CreateDataObject(){
        return new Boolean();
    }
}
class LongFactory extends Factory {
    public Data CreateDataObject(){
        return new Long();
    }
}
class FloatFactory extends Factory {
    public Data CreateDataObject(){
        return new Float();
    }
}
class DoubletFactory extends Factory {
    public Data CreateDataObject(){
        return new Double();
    }
}
//Client classes
class Document {
    Data x;
    Document(Factory q){
        x = q.CreateDataObject();
    }
    public void DisplayData(){
        x.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document a,b,c,d,e,f;
    public static void main(String[] args) {
        a = new Document(new ByteFactory());
        a.DisplayData();
        b = new Document(new ShortFactory());
        b.DisplayData();
        c = new Document(new BoolenFactory());
        c.DisplayData();
        d = new Document(new LongFactory());
        d.DisplayData();
        e = new Document(new FloatFactory());
        e.DisplayData();
        f = new Document(new DoubletFactory());
        f.DisplayData();
    }
}
