public class Complex {
    double RealPart;
    double ImagePart;
    public Complex(){
        RealPart = 0;
        ImagePart = 1;
    }
    public Complex(double R,double I){
        ImagePart = I;
        RealPart = R;
    }
    public boolean equals(Object obj){
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Complex)) {
            return false;
        }
        Complex complex = (Complex) obj;
        if(complex.RealPart != ((Complex) obj).RealPart) {
            return false;
        }
        if(complex.ImagePart != ((Complex) obj).ImagePart) {
            return false;
        }

        return true;
    }
    public String toString()   {
        String string = "";
        if (ImagePart > 0)
            string =  RealPart + "+" + ImagePart + "i";
        if (ImagePart == 0)
            string =  RealPart + "";
        if (ImagePart < 0)
            string = RealPart + " " + ImagePart + "i";
        return string;
    }
    // 定义公有方法:加减乘除
    Complex ComplexAdd(Complex a) {
        double b=0,c=0;
        b = RealPart+a.RealPart;
        c = ImagePart+a.ImagePart;
        System.out.println("("+RealPart+"+"+ImagePart+"i) + ("+a.RealPart+"+"+a.ImagePart+") = "+b+"+"+c+"i");
        return  new Complex(b,c);
    }
    Complex ComplexSub(Complex a) {
        double b=0,c=0;
        b = RealPart-a.RealPart;
        c = ImagePart-a.ImagePart;
        System.out.println("("+RealPart+"+"+ImagePart+"i) - ("+a.RealPart+"+"+a.ImagePart+") = "+b+"+"+c+"i");
        return  new Complex(b,c);
    }
    Complex ComplexMulti(Complex a) {
        double b=0,c=0;
        b = RealPart*a.RealPart;
        c = ImagePart*a.ImagePart;
        System.out.println("("+RealPart+"+"+ImagePart+"i) * ("+a.RealPart+"+"+a.ImagePart+") = "+b+"+"+c+"i");
        return  new Complex(b,c);
    }
    Complex ComplexDiv(Complex a) {
        if(a.RealPart==0||a.ImagePart==0) {
            System.out.println("被除数不能为0，错误");
            return new Complex();
        }

        double d = Math.sqrt(a.RealPart*a.RealPart)+Math.sqrt(a.ImagePart*a.ImagePart);
        double b=0,c=0;
        b = (RealPart*a.RealPart+ImagePart*a.ImagePart)/d;
        c = Math.round((RealPart*a.ImagePart-ImagePart*a.RealPart)/d);
        System.out.println("("+RealPart+"+"+ImagePart+"i) / ("+a.RealPart+"+"+a.ImagePart+") = "+b+"+"+c+"i");
        return  new Complex(b,c);
    }
}