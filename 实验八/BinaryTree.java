import java.util.ArrayList;
import java.util.Iterator;

public interface BinaryTree<T> extends Iterable<T>
{
    public T getRootElement() throws  Exception;

    public BinaryTree<T> getLeft() throws  Exception;

    public BinaryTree<T> getRight() throws Exception;

    public boolean contains (T target) throws Exception;

    public T find (T target) throws  Exception;

    public boolean isEmpty();

    public int size();

    public String toString();

    public ArrayList<T> preorder();

    public Iterator<T> inorder();

    public ArrayList<T> postorder();

    public Iterator<T> levelorder() throws Exception, EmptyCollectionException;
}