import java.util.Scanner;

public class DecisionTree {
    private LinkedBinaryTree<String> tree;

    public DecisionTree(){
        String e1 = "Do you have homework?";
        String e2 = "Do you have any difficulties in study？";
        String e3 = "Are you always hungry？";
        String e4 = "Is it difficult for you to get up？";
        String e5 = "Do you often exercise？";
        String e6 = "Do you like study Java？";
        String e7 = "thank you";
        String e8 = "thank you";
        String e9 = "thank you";
        String e10 = "thank you";
        String e11 = "thank you";
        String e12 = "pass!";
        String e13 = "fail!!!";

        LinkedBinaryTree<String> n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13;
        n8 = new LinkedBinaryTree<String>(e8);
        n9 = new LinkedBinaryTree<String>(e9);
        n4 = new LinkedBinaryTree<String>(e4,n8,n9);
        n10 = new LinkedBinaryTree<String>(e10);
        n11 = new LinkedBinaryTree<String>(e11);
        n5 = new LinkedBinaryTree<String>(e5,n10,n11);
        n12 = new LinkedBinaryTree<String>(e12);
        n13 = new LinkedBinaryTree<String>(e13);
        n6 = new LinkedBinaryTree<String>(e6,n12,n13);
        n7 = new LinkedBinaryTree<String>(e7);
        n2 = new LinkedBinaryTree<String>(e2,n4,n5);
        n3 = new LinkedBinaryTree<String>(e3,n6,n7);

        tree = new LinkedBinaryTree<String>(e1,n2,n3);
    }

    public void diagnose() throws Exception {
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String> current = tree;

        System.out.println("some questions:");
        while(current.size()>1)
        {
            System.out.println(current.getRootElement());
            if(scan.nextLine().equalsIgnoreCase("Y"))
                current = current.getLeft();
            else
                current = current.getRight();
        }
        System.out.println(current.getRootElement());
    }
}