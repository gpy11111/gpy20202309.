import junit.framework.TestCase;

public class LinkedBinaryTreeTest2 extends TestCase {
    private char preorder[]={'A','B','D','H','I','E','J','M','N','C','F','G','K','L'};
    private char inorder[]={'H','D','I','B','E','M','J','N','A','F','C','K','G','L'};
    public void testpostorder(){
        LinkedBinaryTree<Character> linkedBinaryTree=new LinkedBinaryTree<>();
        linkedBinaryTree.initTree(preorder,inorder);
        assertEquals("[H, D, I, B, E, M, J, N, F, C, K, G, L, A]",linkedBinaryTree.postorder(linkedBinaryTree.root));
    }
}