import junit.framework.TestCase;

public class LinkedBinaryTreeTest1 extends TestCase {

    LinkedBinaryTree<Integer> a=new LinkedBinaryTree<>(1);
    LinkedBinaryTree<Integer> b=new LinkedBinaryTree<>(2);
    LinkedBinaryTree<Integer> c=new LinkedBinaryTree<>(3,a,b);
    LinkedBinaryTree<Integer> d=new LinkedBinaryTree<>(23);
    LinkedBinaryTree<Integer> e=new LinkedBinaryTree<>(9);
    LinkedBinaryTree<Integer> f=new LinkedBinaryTree<>(2020,d,e);

    public void testGetRight()throws Exception {
        assertEquals(b.root.element,c.getRight().getRootElement());
        assertEquals(e.root.element,f.getRight().getRootElement());
    }

    public void testContains() {
        assertEquals(true,c.contains(2));
        assertEquals(false,f.contains(10));
    }

    public void testToString() {
        assertEquals("LinkedBinaryTree@299a06ac",c.toString());
        assertEquals("LinkedBinaryTree@383534aa",f.toString());
    }

    public void testPreorder() {
        assertEquals("[3, 1, 2]",c.preorder().toString());
        assertEquals("[2020, 23, 9]",f.preorder().toString());
    }

    public void testPostorder() {
        assertEquals("[1, 2, 3]",c.postorder().toString());
        assertEquals("[23, 9, 2020]",f.postorder().toString());
    }
}