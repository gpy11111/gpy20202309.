import  java.util.*;
public class LinkedBinaryTree<T>
{
    protected BTNode<T> root;

    public LinkedBinaryTree()
    {
        root = null;
    }

    public LinkedBinaryTree (T element)
    {
        root = new BTNode<T>(element);
    }

    public LinkedBinaryTree (T element, LinkedBinaryTree<T> left,
                             LinkedBinaryTree<T> right)
    {
        root = new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    public T getRootElement() throws Exception, EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException ("Get root operation "
                    + "failed. The tree is empty.");

        return root.getElement();
    }

    public LinkedBinaryTree<T> getLeft() throws Exception, EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException ("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getLeft();

        return result;
    }

    public T find (T target) throws ElementNotFoundException {
        BTNode<T> node = null;

        if (root != null)
            node = root.find(target);

        if (node == null)
            throw new ElementNotFoundException("Find operation failed. "
                    + "No such element in tree.");

        return node.getElement();
    }

    //返回大小
    public int size()
    {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }

    public LinkedBinaryTree<T> getRight() throws Exception, EmptyCollectionException {
        if (root == null)
            throw new EmptyCollectionException ("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getRight();

        return result;
    }

    public boolean contains (T target) {
        if (root.find(target)==null){
            return false;
        }
        else {
            return true;
        }
    }

    public boolean isEmpty() {
        if (root==null){
            return true;
        }
        else {
            return false;
        }
    }

    //先序遍历
    public ArrayList<T> preorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.preorder (iter);

        return iter;
    }

    //后续遍历
    public ArrayList<T> postorder(BTNode<Character> root) {
        ArrayList<T> iter = new ArrayList<T>();

        if (this.root != null)
            this.root.postorder (iter);

        return iter;
    }
    public String toString() {
        return super.toString();
    }

    public int findIndexInArray(char[] a, char x, int begin, int end) {
        for(int i=begin;i<=end;i++) {
            if(a[i] == x) {
                return i;
            }
        }
        return -1;
    }

    public void initTree(char[] preOrder, char[] inOrder) {
        this.root = this.initTree(preOrder, 0, preOrder.length-1, inOrder, 0, inOrder.length-1);
    }

    public BTNode initTree(char[] preOrder, int start1, int end1, char[] inOrder, int start2, int end2) {
        if(start1 > end1 || start2 > end2) {
            return null;
        }
        //通过前序找到根结底
        char rootData = preOrder[start1];
        BTNode<Character> head = new BTNode(rootData);
        //从中序遍历里找到根结点所在的位置
        int rootIndex = findIndexInArray(inOrder, rootData, start2, end2);
        //offSet代表左子树的长度-1(也就是中序遍历中，左子树最后一个元素的下标)
        int offSet = rootIndex - start2 - 1;
        //递归构建左子树
        BTNode left = initTree(preOrder, start1+1, start1+1+offSet, inOrder, start2, start2+offSet);
        //递归构建右子树
        BTNode right = initTree(preOrder, start1+offSet+2, end1, inOrder, rootIndex+1, end2);
        head.left = left;
        head.right = right;
        return head;
    }
}