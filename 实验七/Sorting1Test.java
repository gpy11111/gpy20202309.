import junit.framework.TestCase;
import org.junit.Test;
public class Sorting1Test extends TestCase {
    String t1,t2,t3,t4,t5,t6,t7,t8,t9,t10;
    @Test
    public void test1(){
        int[] t = {2309,2,0,345,678};
        t1 = "0 2 345 678 2309 ";
        assertEquals(t1, Sorting1.shellSort_positive(t));
    }
    @Test
    public void test2(){
        int[] t = {11,2309,22};
        t2 = "11 22 2309 ";
        assertEquals(t2, Sorting1.shellSort_positive(t));
    }
    @Test
    public void test3(){
        int[] t = {22,63,2309};
        t3 = "22 66 2309 ";
        assertEquals(t3, Sorting1.shellSort_positive(t));
    }
    @Test
    public void test4(){
        int[] t = {1111,1,9999,2309};
        t4 = "1 1111 2309 9999 ";
        assertEquals(t4, Sorting1.shellSort_positive(t));
    }
    @Test
    public void test5(){
        int[] t = {2,2316,6,3};
        t5 = "2 3 6 2316 ";
        assertEquals(t5, Sorting1.shellSort_positive(t));
    }
    @Test
    public void test6(){
        int[] t = {25,16,2316,8};
        t6 = "2316 25 16 8 ";
        assertEquals(t6, Sorting1.shellSort_inverse(t));
    }
    @Test
    public void test7(){
        int[] t = {9,2316,8};
        t7 = "2316 9 8 ";
        assertEquals(t7, Sorting1.shellSort_inverse(t));
    }
    @Test
    public void test8(){
        int[] t = {2316,10,5,4};
        t8 = "2316 10 5 4 ";
        assertEquals(t8, Sorting1.shellSort_inverse(t));
    }
    @Test
    public void test9(){
        int[] t = {22,2316,9999,6};
        t9 = "9999 2316 22 6 ";
        assertEquals(t9, Sorting1.shellSort_inverse(t));
    }
    @Test
    public void test10(){
        int[] t = {2316,55,9,8};
        t10 = "2316 55 9 8 ";
        assertEquals(t10, Sorting1.shellSort_inverse(t));
    }
}