import junit.framework.TestCase;
import org.junit.Test;

public class SearchingTest extends TestCase {
    @Test
    public void test1(){
        String[] t1 = {" ", "1", "2", "2020", "23", "9"};
        assertEquals(true, Searching.linearSearch(t1, "9"));         //正常
        assertEquals(false, Searching.linearSearch(t1, "11"));        //异常
        assertEquals(true, Searching.linearSearch(t1, "1"));         //边界
    }

    @Test
    public void test2(){
        String[] t1 = {"", "gpy", "2020", "23", "9", "20"};
        assertEquals(true, Searching.linearSearch(t1, "23"));      //正常
        assertEquals(false, Searching.linearSearch(t1, "22"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "gpy"));      //边界
    }

    @Test
    public void test3(){
        String[] t1 = {"", "2309", "2020", "23", "9", "20"};
        assertEquals(true, Searching.linearSearch(t1, "2020"));      //正常
        assertEquals(false, Searching.linearSearch(t1, "33"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "2309"));      //边界
    }

    @Test
    public void test4(){
        String[] t1 = {"", "g", "p", "y", "23", "9"};
        assertEquals(true, Searching.linearSearch(t1, "23"));      //正常
        assertEquals(false, Searching.linearSearch(t1, "44"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "g"));        //边界
    }

    @Test
    public void test5(){
        String[] t1 = {"", "2020", "a", "b", "c", "2309"};
        assertEquals(true, Searching.linearSearch(t1, "2309"));         //正常
        assertEquals(false, Searching.linearSearch(t1, "55"));      //异常
        assertEquals(true, Searching.linearSearch(t1, "2020"));         //边界
    }

}