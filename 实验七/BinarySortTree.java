public class BinarySortTree {
    private Node root;//树的根节点
    //添加节点的方法
    public void add(Node node){
        if (root==null){
            root=node;//如果root为空，直接让root指向node
        }else {
            root.add(node);
        }
    }
    //中序遍历
    public void infixOrder(){
        if (root!=null){
            root.infixOrder();
        }
        else {
            System.out.println("二叉排序树为空，无法遍历");
        }
    }
}

