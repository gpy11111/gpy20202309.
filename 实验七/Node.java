public class Node{
    int value;//存储的元素值
    Node left;//左子树
    Node right;//右子树

    public Node(int value) {//构造方法
        this.value = value;
    }

    @Override
    public String toString() {//为了在遍历时能够清晰地看到遍历结果，重写了toString()方法
        return "Node{" +
                "value=" + value +
                '}';
    }
    //添加节点的方法
    public void add(Node node){
        if (node==null){//待添加的节点为空，就直接return
            return;
        }
        //判断传入的节点的值，和当前的节点的值之间的关系
        if (node.value<this.value){//要插入节点的值<当前节点的值，说明该节点要插到当前节点的左子树

            if (this.left==null){//如果当前节点的左子节点为空，直接把插入的节点作为当前节点的左子树
                this.left=node;
            }else {//如果当前节点的左子节点不为空，递归向左子树添加
                this.left.add(node);
            }
        }else {//要插入节点的值>=当前节点的值,，说明该节点要插到当前节点的右子树
            if (this.right==null){//如果当前节点的右子节点为空，直接把插入的节点作为当前节点的右子树
                this.right=node;
            }else {//如果当前节点的左子节点不为空，递归向右子树添加
                this.right.add(node);
            }
        }
    }
    public void infixOrder(){
        if (this.left!=null){
            this.left.infixOrder();
        }
        System.out.println(this);
        if (this.right!=null){
            this.right.infixOrder();
        }
    }

}
