import junit.framework.TestCase;

public class Search1Test extends TestCase {
    private Search1 Searching2;

    public void testlinearSearch(){                                                //由于设置了哨兵，所以边界index为1
        String[] t = {" ", "1", "2", "2316", "3", "4"};
        assertEquals(true, Searching.linearSearch(t, "2"));         //正常
        assertEquals(false, Searching.linearSearch(t, "0"));        //异常
        assertEquals(true, Searching.linearSearch(t, "1"));         //边界
    }

    public void testSearch11(){
        int[] t = {1, 16, 19, 20, 2316};
        assertEquals(4, Searching2.Search11(t, 2316, 0, 4));
    }

    public void testSearch22(){
        int[] t = {1, 16, 19, 20, 2316};
        assertEquals(1, Searching2.Search22(t, 16, 0, 4));
    }

    public void testFibonacci(){
        int[] t = {1, 16, 19, 20, 2316};
        assertEquals(1, Searching2.fibonacciSearching(t, 16, t.length));
    }

    public void testSearch55(){
        int[] t = {1, 2, 3, 4, 2316, 11, 99, 16, 19, 20, 23};
        assertEquals(6, Searching2.Search55(t, 99));              //正常
        assertEquals(-1, Searching2.Search55(t, 9999));           //异常
    }

    public void testSearch44(){
        int index[]={22,48,86};
        int[] t = {22, 12, 13, 8, 9, 20,  33, 42, 44, 38, 24, 48,  60, 58, 74, 49, 86, 53};
        assertEquals(15, Searching2.Search44(index, t, 49, 6));       //正常
        assertEquals(11, Searching2.Search44(index, t, 48, 6));       //边界
    }
}