import java.util.Arrays;
public class Search1{
    //二分
    public static <T> int Search11(int a[], int value, int low, int high) {
        int mid = low + (high - low) / 2;
        if(a[mid] == value)
            return mid;
        else if(a[mid] > value)
            return Search11(a, value, low, mid - 1);
        else if(a[mid] < value)
            return Search11(a, value, mid + 1, high);
        else return -1;
    }
    //插值
    public static <T> int Search22(int a[], int value, int low, int high)
    {
        int mid = low+(value - a[low]) / (a[high] - a[low]) * (high - low);
        if(a[mid] == value)
            return mid;
        else if(a[mid] > value)
            return Search22(a, value, low, mid-1);
        else if(a[mid] < value)
            return Search22(a, value, mid+1, high);
        else return 0;
    }
    //斐波那契
    public static void Fibonacci(int F[]){
        int maxSize = 20;
        F[0] = 0;
        F[1] = 1;
        for (int i = 2; i < maxSize; i++){
            F[i] = F[i - 1] + F[i - 2];
        }
    }
    public static <T> int fibonacciSearching(int a[], int target, int size){
        int low = 0;
        int high = size - 1;
        int maxSize = 20;
        int F[] = new int[maxSize];
        Fibonacci(F);
        int k = 0;
        while (size > F[k] - 1){
            k++;
        }

        int temp[]= Arrays.copyOf(a, F[k]);

        for(int i = size; i < F[k] - 1; ++i){
            temp[i] = a[size - 1];
        }

        while(low <= high) {
            int mid = low + F[k - 1] - 1;
            if (target < temp[mid]) {
                high = mid - 1;
                k -= 1;
            } else if (target > temp[mid]) {
                low = mid + 1;
                k -= 2;
            } else {
                if (mid < size)
                    return mid;
                else
                    return size - 1;
            }
        }
        return -1;
    }
    //分块
    public static int Search44(int[] index,int[]t,int key,int m){
        int i = search(index,key);
        if(i >= 0){
            int j = m*i;
            int site = (i + 1) * m;
            for( ; j < site; j++){
                if(t[j] == key)
                    return j;
            }
        }
        return -1;
    }
    public static int search(int[]index,int keytype){
        int i;
        for(i = 0; ; i++){
            if(index[i] >= keytype){
                break;
            }
        }
        return i;
    }
    //哈希
    public static <T> int Search55(int[] a, int key){
        int m = key % 11;
        while(a[m] != key && m < a.length - 1){
            m++;
        }
        if(a[m] != key){
            return -1;
        }
        else{
            return m;
        }
    }
}
