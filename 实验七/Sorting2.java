import java.util.Arrays;
public class Sorting2{
    public static void main(String[] args) {
        int[] arr = new int[]{4,6,8,5,9};
        int length = arr.length;
        //从最后一个非叶节点开始构建大顶堆
        for (int i = arr.length/2-1; i >=0; i--) {
            maximumHeap(i,arr,length);
        }
        //从最小的叶子节点开始与根节点进行交换并重新构建大顶堆
        for (int i = arr.length-1; i >=0; i--) {
//            System.out.println(Arrays.toString(arr));
            swap(arr,0,i);
            length--;
            maximumHeap(0,arr,length);
        }
        System.out.println(Arrays.toString(arr));
    }
    //构建大顶堆
    public static void maximumHeap(int i,int[] arr,int length){
        int temp = arr[i];
        for (int j = i*2+1; j < length; j=j*2+1) {
            if(j+1<length && arr[j+1]>arr[j]){
                j++;
            }
            if(arr[j]>temp){
                arr[i] = arr[j];
                i = j;
            }else{
                break;
            }
        }
        //将temp放到最终位置
        arr[i] = temp;
    }
    //交换
    public static void swap(int[] arr,int i,int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
